﻿using System.CommandLine;
using DepSketch.Arguments;
using DepSketch.Options;
using DepSketch.Services;

var rootCommand = new RootCommand();

var diagramArgument = Arguments.CreateDiagramTypeArgument();
var projectOption = Options.CreateProjectOption();
var directoryOption = Options.CreateDirectoryOption();
var listOption = Options.CreateListOption();
var outputOption = Options.CreateOutputOption();

rootCommand.Add(diagramArgument);

rootCommand.Add(projectOption);
rootCommand.Add(directoryOption);
rootCommand.Add(listOption);
rootCommand.Add(outputOption);

rootCommand.Description = "CLI tool to generate diagrams that show the dependencies from .Net projects";

rootCommand.SetHandler((diagramTypeArgumentValue, projectOptionValue, directoryOptionValue, listOptionValue,
    outputOptionValue) =>
{
    var service = new DiagramService();
    
    if (string.IsNullOrEmpty(projectOptionValue) && string.IsNullOrEmpty(directoryOptionValue) &&
        string.IsNullOrEmpty(listOptionValue))
    {
        Console.WriteLine("Error: At least one of the options (--project, --dir, --list) must be provided.");
        return Task.CompletedTask;
    }

    if (!string.IsNullOrEmpty(projectOptionValue) && !File.Exists(projectOptionValue))
    {
        Console.WriteLine("Error: The provided project does not exist");
        return Task.CompletedTask;
    }

    service.ProjectFile = projectOptionValue;

    if (!string.IsNullOrEmpty(directoryOptionValue) && !Directory.Exists(directoryOptionValue))
    {
        Console.WriteLine("Error: The provided directory does not exist.");
        return Task.CompletedTask;
    }

    service.DirectoryPath = directoryOptionValue;

    if (!string.IsNullOrEmpty(listOptionValue)&& !File.Exists(listOptionValue))
    {
        Console.WriteLine("Error: The provided list does not exist");
        return Task.CompletedTask;
    }

    service.ListPath = listOptionValue;
    service.OutputPath = outputOptionValue;
    service.GenerateDiagram(diagramTypeArgumentValue);
    
    return Task.CompletedTask;
}, diagramArgument, projectOption, directoryOption, listOption, outputOption);

await rootCommand.InvokeAsync(args);