﻿using System.CommandLine;

namespace DepSketch.Options;

public static class Options
{
    public static Option<string> CreateProjectOption()
    {
        return new Option<string>(
            "--project",
            "Path to a csproj file that should be analyzed.")
        {
            IsRequired = false
        };
    }

    public static Option<string> CreateDirectoryOption()
    {
       return new Option<string>(
            "--dir",
            "Path to a directory that contains at least one csproj file")
        {
            IsRequired = false
        };
    }
    
    public static Option<string> CreateListOption()
    {
       return new Option<string>(
            "--list",
            "Path to a textfile that contains a list with paths to csproj files")
        {
            IsRequired = false
        };
    }
    
    public static Option<string> CreateOutputOption()
    {
        return new Option<string>(
            "--output",
            "Output directory")
        {
            IsRequired = true
        };
    }
}