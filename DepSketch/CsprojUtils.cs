using System.Xml;

namespace DepSketch;

public static class CsprojUtils
{
    public static List<string> FindCsprojFiles(string folderPath)
    {
        var csprojFiles = new List<string>();
        try
        {
            var files = Directory.GetFiles(folderPath);

            csprojFiles.AddRange(files.Where(file => Path.GetExtension(file).Equals(".csproj", StringComparison.Ordinal)));

            var directories = Directory.GetDirectories(folderPath);

            foreach (var directory in directories)
            {
                csprojFiles.AddRange(FindCsprojFiles(directory));
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"An error occured while searching for .csproj files: {ex.Message}");
        }

        return csprojFiles;
    }

    public static Dictionary<string, string> ReadPackageReferences(string csprojFile)
    {
        var packageReferences = new Dictionary<string, string>();

        try
        {
            var document = new XmlDocument();
            document.Load(csprojFile);

            var packageReferenceNodes = document.SelectNodes("//PackageReference");
            if (packageReferenceNodes != null)
            {
                foreach (XmlNode node in packageReferenceNodes)
                {
                    var packageName = node.Attributes?["Include"]?.Value;
                    var packageVersion = node.Attributes?["Version"]?.Value;

                    if (packageName == null) continue;
                    if (packageVersion != null)
                        packageReferences.Add(packageName, packageVersion);
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"An error occurred while reading the XML file: {ex.Message}");
        }

        return packageReferences;
    }
}