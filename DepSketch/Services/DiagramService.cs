using System.Text;

namespace DepSketch.Services;

public class DiagramService
{
    public string OutputPath { get; set; }
    public string ProjectFile { get; set; }
    public string DirectoryPath { get; set; }
    public string ListPath { get; set; }

    private List<string> _projects { get; set; } = [];
    private const string DIAGRAMNAME = "autogenerated_diagramm.md";
    private string _diagram;

    private readonly Dictionary<string, Dictionary<string, string>> _dependencies =
        new Dictionary<string, Dictionary<string, string>>();

    public void GenerateDiagram(string diagramTypeArgumentValue)
    {
        if (diagramTypeArgumentValue.Equals("plantuml"))
        {
            Console.WriteLine("PlantUml is not implemented yet");
        }

        GatherAllProjectFiles();
        AnalyzeProjects();
        BuildDiagramString();
        WriteDiagramToFile();
    }

    private void WriteDiagramToFile()
    {
        File.WriteAllText(Path.Combine(OutputPath, DIAGRAMNAME), _diagram);
    }

    private void BuildDiagramString()
    {
        _diagram = DiagramHelper.GenerateMermaidDiagramHeader();
        var sb = new StringBuilder();
        foreach (var dependency in _dependencies)
        {
            sb.Append(DiagramHelper.GenerateMermaidDiagramDependencies(dependency.Key, dependency.Value));
        }

        _diagram += sb.ToString();

        _diagram += DiagramHelper.GenerateMermaidDiagramFooter();
    }

    private void AnalyzeProjects()
    {
        foreach (var project in _projects)
        {
            var projectName = Path.GetFileNameWithoutExtension(project);
            var dependencies = CsprojUtils.ReadPackageReferences(project);
            _dependencies.Add(projectName, dependencies);
        }
    }

    private void GatherAllProjectFiles()
    {
        if (!string.IsNullOrEmpty(ProjectFile))
        {
            _projects.Add(ProjectFile);
        }

        if (!string.IsNullOrEmpty(DirectoryPath))
        {
            _projects.AddRange(CsprojUtils.FindCsprojFiles(DirectoryPath));
        }

        if (string.IsNullOrEmpty(ListPath)) return;
        var listOfProjects = File.ReadAllLines(ListPath);
        _projects.AddRange(listOfProjects);
    }
}