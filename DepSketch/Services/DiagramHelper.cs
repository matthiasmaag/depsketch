using System.Text;

namespace DepSketch.Services;

public static class DiagramHelper
{
    public static string GenerateMermaidDiagramHeader()
    {
        var sb = new StringBuilder();
        sb.AppendLine("```mermaid");
        sb.AppendLine("flowchart LR");
        sb.AppendLine();
        return sb.ToString();
    }

    public static string GenerateMermaidDiagramFooter()
    {
        var sb = new StringBuilder();
        sb.AppendLine("```");
        return sb.ToString();
    }

    public static string GenerateMermaidDiagramDependencies(string projectName, Dictionary<string, string> deps)
    {
        var sb = new StringBuilder();
        foreach (var dep in deps)
        {
            sb.AppendLine("    " + projectName + " -.-> " + dep.Key + "_" + dep.Value);
        }

        return sb.ToString();
    }
}