﻿using System.CommandLine;

namespace DepSketch.Arguments;

public static class Arguments
{
    public static Argument<string> CreateDiagramTypeArgument()
    {
        var diagramTypeArgument = new Argument<string>("DiagramType", "Type of diagram to generate (mermaid or plantuml)")
        {
            Arity = ArgumentArity.ExactlyOne
        };

        diagramTypeArgument.AddValidator(symbol =>
        {
            var value = symbol.GetValueOrDefault<string>();

            if (value != "mermaid" && value != "plantuml")
            {
                Console.WriteLine($"Invalid diagram type '{value}'. DiagramType must be 'mermaid' or 'plantuml'.");
            }
        });

        return diagramTypeArgument;
    }
}