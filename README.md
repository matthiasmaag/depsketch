# DepSketch

DepSketch is a CLI tool to generate diagrams that show the dependencies from .NET projects. It supports generating diagrams in Mermaid format.

## Features

- Generate Mermaid diagrams for .NET project dependencies.
- Supports specifying individual project files, directories, or lists of projects.

## Installation

To install DepSketch, clone the repository and build the project using your preferred .NET build tool.

```bash
git clone <repository-url>
cd depsketch
dotnet build
```

## Usage

DepSketch can be used from the command line. Below are the available options and arguments:

### Arguments

- **diagramTypeArgument**: Specifies the type of diagram to generate. Currently, only mermaid is supported.

### Options

- **--project**: Specifies a single project file to analyze.
- **--dir**: Specifies a directory containing multiple project files to analyze.
- **--list**: Specifies a file containing a list of project files to analyze.
- **--output**: Specifies the output directory for the generated diagram.

## Example Commands

Generate a Mermaid diagram for a single project:

```bash
dotnet run -- --project path/to/project.csproj --output path/to/output
```

Generate a Mermaid diagram for all projects in a directory:

```bash
dotnet run -- --dir path/to/projects --output path/to/output
```

Generate a Mermaid diagram for a list of projects:

```bash
dotnet run -- --list path/to/list.txt --output path/to/output
```

The text file containing the list of projects should contain the full path to each project file, separated by newlines.

## Contributing

Contributions are welcome! Please open an issue or submit a pull request.

## License

This project is licensed under the MIT License. See the [LICENSE](license) file for details.